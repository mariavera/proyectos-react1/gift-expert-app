export const getGifs = async (query) => {
    const api_key = 'rw8u30qx6v0UQWj29GSsn8bplKm2KOMJ';
    const url = `https://api.giphy.com/v1/gifs/search?api_key=${api_key}&q=${encodeURI(query)}&limit=10`;
    const resp = await fetch(url);
    const { data } = await resp.json();
    const gifs = data.map(
        img => {
            return {
                id: img.id,
                title: img.title,
                url: img.images.downsized_medium.url
            }
        }
    )

    return gifs;
}

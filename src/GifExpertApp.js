import React, { useState } from 'react';
import { AddCategory } from './components/AddCategory';
import { GifGrid } from './components/GifGrid';

export const GifExpertApp = () => {
    // const categorias = ['One Punch', 'Samurai X', 'Dragon Ball']
    const [categories, setcategories] = useState(['Shingeki no kyojin']);
    // const handleAdd = () => {
    // setcategories( [...categories,'Mulan'])
    // }
    return (
        // <><pre>{JSON.stringify(saludo, null, 3)}</pre>
        <><h2>GifExpertApp</h2>
            <AddCategory setcategories={setcategories}/>
            <hr />
            <ol>
                {
                    categories.map((category) => 
                  <GifGrid 
                  key= {category}
                  category={category}/>
                )
                }
            </ol>
        </>

    );

}

